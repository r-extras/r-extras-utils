% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/transform.R
\name{to_json}
\alias{to_json}
\title{Tweaked \code{toJSON} function}
\usage{
to_json(value, ...)
}
\arguments{
\item{value}{\code{RObject} to be converted}

\item{...}{Additional paramenters for \code{jsonlite::toJSON}}
}
\value{
JSON string
}
\description{
Tweaked \code{toJSON} function
}
