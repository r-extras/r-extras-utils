# `r.extras.utils`

## Helper library with miscellaneous common functions.

### Installation from R console
>
> `> remotes::install_gitlab("r-extras/r-extras-utils")`
>

### Dependency inclusion

Include also repository at `DESCRIPTION` file under `Remotes` entry.

```yaml
...
Imports:
    ...
    r.extras.utils
...
Remotes:
    ...
    gitlab::r-extras/r-extras-utils
...
```

### Package download

Created versions are available in the [package registry](https://gitlab.com/r-extras/r-extras-utils/-/packages)

### Licence

SPDX [MIT](LICENSE) Licence.

***
