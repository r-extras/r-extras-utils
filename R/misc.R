# Ungrouped helper miscelaneous functions

#' drop_nulls from shinyWidgets/utils.R
#' @param x \code{Robject} to remove NULL elements from
#' @return modified \code{x}
#' @export
drop_nulls <- function(x) {
    x[!vapply(x, is.null, FUN.VALUE = logical(1))]
}

#' Retrieve files from package folder
#'
#' Recover a list of file paths filtered by extension.
#'  Paths are absolute to specific package folder.
#'  Hidden files are ignored and names correspond to
#'  the filename without extension.
#'
#' @param package \code{character} installed package name
#' @param extension \code{character} extension of listed files without dot
#' @param ... path elements of package folder after \code{inst}
#' @returns \code{list} of paths to files named with filename without extension
#' @export
files_from_pkg <- function(package, extension, ...) {
    extension <- as.character(extension[1L])
    if (regexpr("[A-z0-9]{1,}", extension) != 1L)
        stop("Extension must be alphanumeric, remove dot if present")
    extension <- sprintf(".%s", extension)
    files <- list.files(
        system.file(..., package = package[1L]),
        full.names = TRUE
    )
    files <- files[which(endsWith(files, extension))]
    if (length(files) == 0L)
        stop("Files not found at package folder")
    names <- basename(files) |>
        stringi::stri_replace_last_fixed(extension, "")
    structure(
        as.list(files),
        names = names
    )
}

#' Find if all elements can be coherced to integers.
#'
#' Elements that convert to \code{NA} and code{NaN} are also non-valid
#'
#' @param x \code{vector} to check
#' @return \code{logical} of the same length as \code{x}
#' @export
int_able <- function(x)
    vapply(
        suppressWarnings(as.integer(x)),
        function(i) {
            if (is.na(i)) return(FALSE)
            is.integer(i) & !is.nan(i)
        },
        logical(1)
    )

#' Create integer sequence based on object length
#' @param x object to extract length of
#' @return `integer` vector, 1L:length(x)
#' @export
seq_length <- function(x)
    if (!length(x)) NULL else seq_len(length(x))
